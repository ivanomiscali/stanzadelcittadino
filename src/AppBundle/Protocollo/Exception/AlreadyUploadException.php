<?php

namespace AppBundle\Protocollo\Exception;

class AlreadyUploadException extends BaseException
{
    protected $message = 'File already upload';

}
